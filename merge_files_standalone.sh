#!/bin/bash
####################################################################################
#                             Merge PFEA files 
####################################################################################
cd /home/carvajal/InputProcess/InputOPSOL/
FicompleteOPSOL=$(ls | wc -l)
FicompleteJNTS=$(ls /home/carvajal/InputProcess/InputJNTS/ | wc -l)
if [ "$FicompleteOPSOL" = 5 ] && [ "$FicompleteJNTS" = 5 ];
then
mv /home/carvajal/OutputProcess/*.txt /home/carvajal/OutputProcess/OutputBack/
cd /home/carvajal/InputProcess/InputOPSOL/
CCMname=$(ls ccmo*)
EXTname=$(ls EXTP*)
DETname=$(ls Detalle*)
TRAname=$(ls TRANSF*)
cd /home/carvajal/InputProcess/InputJNTS
echo `date` Los Archivos en directorio son :  OPSOL- $FicompleteOPSOL  y  JNTS- $FicompleteJNTS ...Inciando Combinacion
sed '$d' /home/carvajal/InputProcess/InputJNTS/EXTP*.txt > file1.txt
sed '1d' /home/carvajal/InputProcess/InputOPSOL/EXTP*.txt > file2.txt
F1lines=$(more /home/carvajal/InputProcess/InputJNTS/EXTP*.txt | wc -l)
F2lines=$(more /home/carvajal/InputProcess/InputOPSOL/EXTP*.txt | wc -l) 
cat file1.txt file2.txt > ResultFile.txt
PTsize=$(more ResultFile.txt | wc -l)
DTsize=$(expr $PTsize - 2)
DRsize=$(expr $F2lines - 2)
###Agragando echos para ver
echo $DTsize
echo $DRsize
###########################
sed -i "\$s/$DRsize/$DTsize/" ResultFile.txt
CUTsize=$(tail -n 2 ResultFile.txt | sed '$d' | wc -m)
CUTsize1=$(expr $CUTsize - 2) 
sed '$d' ResultFile.txt > ResultFile2.txt
tail -n 1 ResultFile.txt | awk '{print substr($1,1,550)}' >> ResultFile3.txt
echo `cat ResultFile3.txt`$'\r' >> ResultFile2.txt
cp  ResultFile2.txt ../../OutputProcess/$EXTname
rm file1.txt file2.txt ResultFile*.txt
#sed '$d' /home/carvajal/InputProcess/InputJNTS/ccmo*.txt > /home/carvajal/OutputProcess/ccmofecha.txt 
cat /home/carvajal/InputProcess/InputOPSOL/ccmo*.txt /home/carvajal/InputProcess/InputJNTS/ccmo*.txt > /home/carvajal/OutputProcess/$CCMname.pre
cat /home/carvajal/InputProcess/InputJNTS/Detalle*.txt /home/carvajal/InputProcess/InputOPSOL/Detalle*.txt > /home/carvajal/OutputProcess/$DETname
cat /home/carvajal/InputProcess/InputJNTS/TRANSF*.txt /home/carvajal/InputProcess/InputOPSOL/TRANSF*.txt >  /home/carvajal/OutputProcess/$TRAname
Ccmosize=$(cat /home/carvajal/OutputProcess/$CCMname.pre | wc -l)
for (( i=1; i <= "$Ccmosize" ; i++ ))
do
  # awk '{ gsub("\|","*"); print $0 }' ccmo*.txt | awk '{$20='$i';print}'
  awk 'BEGIN { FS = "|" } ; NR=='$i' { $51='$i';print}' /home/carvajal/OutputProcess/$CCMname.pre | awk '{ gsub(" ","|");print}' >> /home/carvajal/OutputProcess/ccmo1.pre
  # awk 'BEGIN { FS = "|" } ; { $51='$i';print $0 }' ccmo*.txt >> comosuccess.txt
done
awk 'BEGIN { FS = "|"; OFS = "|" } ; NR==1 { l=$11;} {$11=l;print}' /home/carvajal/OutputProcess/ccmo1.pre > /home/carvajal/OutputProcess/ccmo2.pre
mv /home/carvajal/OutputProcess/ccmo2.pre /home/carvajal/OutputProcess/$CCMname
rm /home/carvajal/OutputProcess/$CCMname.pre /home/carvajal/OutputProcess/ccmo1.pre
mv /home/carvajal/InputProcess/InputOPSOL/*.txt /home/carvajal/InputProcess/InputOPSOL/InputBack/
mv /home/carvajal/InputProcess/InputJNTS/*.txt /home/carvajal/InputProcess/InputJNTS/InputBack/
cd /home/carvajal/OutputProcess/
for old in *.txt;
do
iconv -f ISO-8859-1 -t ASCII//TRANSLIT $old >  $old.ascii
mv $old.ascii $old
done

else
echo `date` Existen $FicompleteOPSOL archivos en OPSOL y $FicompleteJNTS en JNTS, no corresponde al numero necesario, 5-5...Intentando mas tarde.
fi

